pragma solidity ^0.4.4;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/Refugee.sol";

contract TestRefugee {
    Refugee refugee = Refugee(DeployedAddresses.Refugee());

    function testGetVaccined() {
    	// create 16 spoofed (time,address) mappings

	for (uint i=0;i<15;i++){
	    uint tempTimestamp = (i+12)*34;
	    address tempBlockchainAddress = 0xdead;
	    uint tempReturnedNumber = refugee.getVaccined(tempTimestamp, tempBlockchainAddress);
	    Assert.equal(tempReturnedNumber,i+1, "Missed vaccine history!");
	}
	// insert one more here, which should fail
	uint tempTime = 178934;
	address tempFailAddress = 0xcafe;
	uint returnedNumber = refugee.getVaccined(tempTime, tempFailAddress);
    }
}