pragma solidity ^0.4.4;

contract Refugee {
    // address on the blockchain of a refugee
    address public blockchainId;

    // total vaccine times: maximum is 16
    uint public totalVaccineTime;

    // vaccine history
    // (timestamp, UN station address)
    // maximum vaccine number is 16
    mapping (uint => address) refugeeVaccineHistory;

    function assignAddress() public returns (address) {
        blockchainId = msg.sender;
	return blockchainId;
    }

    function getVaccined(uint _vaccineTimestamp, address _UNstation) public returns (uint) {
    	require(totalVaccineTime >= 0 && totalVaccineTime <= 14);
	refugeeVaccineHistory[_vaccineTimestamp] = _UNstation;
	totalVaccineTime ++;
	return totalVaccineTime;
    }

}