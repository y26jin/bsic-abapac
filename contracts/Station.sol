pragma solidity ^0.4.4;

contract Station {
    // address on the blockchain of an UN station
    address public stationBlockchainId;

    // total refugee capacity: maximum is 50
    uint public totalRefugeeCapacity;

    // refugee vaccine history
    //mapping (address => )

    function registerOnBlockchain() public returns (address) {
        stationBlockchainId = msg.sender;
	return stationBlockchainId;
    }

}